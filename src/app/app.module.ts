import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ParentComponent } from './components/parent/parent.component';
import { PathSelectionComponent } from './components/path-selection/path-selection.component';
import {Routes, Router, RouterOutlet, RouterModule} from "@angular/router"
const appRoutes: Routes = [
  { path: 'parent', component: ParentComponent },
  { path: '', component: PathSelectionComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    ParentComponent,
    PathSelectionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(
      appRoutes
      //,{ enableTracing: true } // <-- debugging purposes only
    )
  ] ,
  exports:[],
  providers: [RouterModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
