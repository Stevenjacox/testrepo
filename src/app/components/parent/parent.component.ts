import { Component, OnInit } from '@angular/core';
import * as Survey from 'survey-angular';
import {Router} from '@angular/router';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit {

  private surveyComplete: boolean = false;

  constructor(private _router: Router) { 
    
  }
  
  surveyChanged()
  {
      console.log("surveyChanged");
      this._router.navigateByUrl("");
  }
  
  ngDoCheck() {
    
  }

  ngOnInit() {

    
    const json = {
      
      showQuestionNumbers: "off",
      goNextPageAutomatic: true,
      
      pages:[
        {
      
      questions: [
          {
              type: "dropdown",
              name: "indefault",
              title: "Is your loan in default?",
              isRequired: true,
              choices: ["Yes", "No"]
          }
        ] }, {
        questions: [
        
          {
              type: "dropdown",
              name: "isloanhigherthan20percent",
              title: "Is your loan amount higher than 20% of your discretional income?",
              visibleIf: "{indefault} = 'No'",
              isRequired: true,
              choices: [
                  "Yes", "No"
              ]
          }
        ] }, { 
          questions: [
          {
              type: "text",
              name: "refinanceloan",
              title: "Good news! We can help you refinance your loan! Enter your email and click complete below so we can get started!",
              visibleIf: "{indefault} = 'No' && {isloanhigherthan20percent}='Yes'",
          }
        ] }, {
          questions: [
          {
            type: "text",
            name: "consolidateloan",
            title: "Good news! We can help you consolidate your loans into a Direct Consolidation Loan! Enter your email and click complete below so we can get started!",
            visibleIf: "{indefault} = 'No' && {isloanhigherthan20percent}='No'",
        } 
          
      ]
    }
],
completedHtml: ""
};

  let surveyModel = new Survey.Model(json);
    Survey.SurveyNG.render('surveyElement', { model: surveyModel });
  
   
  surveyModel
  .onComplete
  .add(function (result) {
      
      let newresult = result.data;
      newresult.isParent = true;
      document
      .querySelector('#surveyResult')
      .innerHTML = "result: " + JSON.stringify(newresult);

    });
    
   
    
      
 
  
  
  }
  

}
