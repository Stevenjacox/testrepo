Survey
    .StylesManager
    .applyTheme("default");

var json = {
    showQuestionNumbers: "off",
    questions: [
        {
            type: "radiogroup",
            name: "firstquestion",
            title: "Are you any of the following? Please select one.",
            isRequired: true,
            choices: [
                "Parent", "Teacher", "Lawyer", "In the medical profession", "Other public servant", "None of these"
            ],
            colCount: 0
        }, {
            type: "dropdown",
            name: "indefault",
            title: "Is your loan in default?",
            visibleIf: "{firstquestion} != undefined",
            isRequired: true,
            choices: ["Yes", "No"]
        }, {
            type: "dropdown",
            name: "isloanabove20k",
            title: "Is your loan above $20000 or will you be able to pay it off in 3-5 years?",
            visibleIf: "{indefault} = 'No' && {firstquestion} != 'None of these'",
            isRequired: true,
            choices: [
                "Yes", "No"
            ]
        },{
            type: "dropdown",
            name: "isloanhigherthanannual",
            title: "Is your loan amount higher than your annual salary?",
            visibleIf: "{indefault} = 'No' && {firstquestion} = 'None of these'",
            isRequired: true,
            choices: [
                "Yes", "No"
            ]
        } ,
        {
            type: "dropdown",
            name: "ismarried",
            title: "Are you married?",
            visibleIf: "{indefault} = 'No' && {firstquestion} = 'Parent'",
            isRequired: true,
            choices: [
                "Yes", "No"
            ]
        } ,
        {
            type: "dropdown",
            name: "isloanhigherthan20percent",
            title: "Is your loan amount higher than 20% of your discretional income?",
            visibleIf: "{indefault} = 'No' && {firstquestion} = 'Parent'",
            isRequired: true,
            choices: [
                "Yes", "No"
            ]
        } ,
        {
            type: "dropdown",
            name: "isstruggling",
            title: "Are you struggling to make your monthly payment?",
            visibleIf: "{indefault} = 'No' && {firstquestion} = 'None of these' && {isloanhigherthan20percent} = 'Yes'",
            isRequired: true,
            choices: [
                "Yes", "No"
            ]
        } 
        
    ]
};

window.survey = new Survey.Model(json);

survey
    .onComplete
    .add(function (result) {
        document
            .querySelector('#surveyResult')
            .innerHTML = "result: " + JSON.stringify(result.data);
    });

function onAngularComponentInit() {
    console.log("Testing")
    Survey
        .SurveyNG
        .render("surveyElement", {model: survey});
}
var HelloApp = ng
    .core
    .Component({selector: 'ng-app', template: '<div id="surveyContainer" class="survey-container contentcontainer codecontainer"><div id="surveyElement"></div></div> '})
    .Class({
        constructor: function () {},
        ngOnInit: function () {
            onAngularComponentInit();
        }
    });
document.addEventListener('DOMContentLoaded', function () {
    ng
        .platformBrowserDynamic
        .bootstrap(HelloApp);
});

